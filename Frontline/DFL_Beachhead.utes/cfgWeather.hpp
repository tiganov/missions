class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0.3,0.8}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 0;
	fogFrame[] = {0,0.09}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {5.7,5.7}; // [0,24]
};