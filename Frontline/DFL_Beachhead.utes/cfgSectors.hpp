class CfgSectors {
    class base_west {
        designator = "Beach Landing";
        sectortype = "mainbase";
        bleedValue = 0;
    };

	class base_west_1 {
        designator = "Carrier";
        sectortype = "mainbase";
        bleedValue = 0;
        spawnMarker = "baseSpawn_west_1";
        spawnHeight = 15;
    };

    class base_east {
        designator = "Beach Landing";
        sectortype = "mainbase";
        bleedValue = 0;
    };
};
