#!/usr/bin/env python3

import os
import sys
import shutil
import fileinput

from distutils.dir_util import copy_tree

##########################
SQUADSETS = {
    'USA': "RHS_USMC_Woodland",
    'RU': "RHS_RUA_Woodland",
    'NATO': "Apex_NATO",
    'PRC': "Apex_China",
    'Wehrmacht': "IFA_Wehr",
    'Red Army': "IFA_USSR",
    'U.S. Army': "IFA_USA",
    'INS': "RHS_Insurgents_Desert"
}

# This will add the Squads entry to missions that don't have it yet
def update_file(filepath):
    found_line = False

    needle = "squads"
    squadset = ""
    with open(filepath, "r") as fileread:
        lines = fileread.readlines()
        for line_index, line_text in enumerate(lines):
            if needle in line_text:
                print('File already contains {} entry'.format(needle))
                break
        else: # not found, we are at the eof
            with open(filepath, "w") as filewrite:
                for line_index, line_uwot in enumerate(lines):
                    line_uwot = line_uwot.rstrip('\n')
                    if line_uwot.startswith('        name ='):
                        # Here i wanna read what's behind name =, then get a squadset for that name
                        right_side = line_uwot.split('=')[1]
                        factionname = right_side.strip().rstrip(';').strip('""')

                        squadset = SQUADSETS.get(factionname, "")
                        print('Found name entry: {} - {}'.format(factionname, squadset))
                        found_line = True
                    else:
                        if found_line:
                            print('        squads = "{}";'.format(squadset), file=filewrite),
                            found_line = False
                    print(line_uwot, file=filewrite),

def missions_walk(missionspath, copypath):
    os.chdir(missionspath)
    for o_value in os.listdir(missionspath):
        if o_value == "Archive":
            continue

        subpath = os.path.join(missionspath, o_value)

        for p_value in os.listdir(subpath):
            path = os.path.join(missionspath, subpath, p_value)
            if not os.path.isdir(path):
                continue
            if p_value.startswith("."):
                continue
            if p_value.startswith("DFL_"):
                continue

            missionfile = os.path.join(path, "mission.sqm")
            if not os.path.isfile(missionfile):
                continue

            print("# Updating {} ...".format(p_value))

            copy_tree(copypath, path)

            # Delete old Squads folders
            squadsfolder = os.path.join(path, "Squads")
            if os.path.isdir(squadsfolder):
                shutil.rmtree(squadsfolder)

            update_file(os.path.join(path, "cfg", "cfgSides.hpp"))


def update_missions():
    print("""
  ###################
  # Frontline Build #
  ###################
""")


    scriptpath = os.path.realpath(__file__)
    projectpath = os.path.dirname(os.path.dirname(scriptpath))
    copypath = os.path.join(projectpath, "tools", "missionupdate")
    missionspath = os.path.join(projectpath, "Frontline")

    print("# Updating {} ...".format(missionspath))
    print("# Updating {} ...".format(copypath))
    print("###################")

    missions_walk(missionspath, copypath)

if __name__ == "__main__":
    sys.exit(update_missions())
